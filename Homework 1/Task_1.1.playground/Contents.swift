import UIKit


func arithmeticMean(_ numbers: Int...) -> Double {
    return Double(numbers.reduce(0, +)) / Double(numbers.count)
}

func geometricMean(_ numbers: Int...) -> Double {
    return pow(Double(numbers.reduce(Int(1.0), *)), 1.0 / Double(numbers.count))
}


// Simple tests
let result1 = arithmeticMean(5, 10, 15, 20)
let result2 = geometricMean(4, 10, 16, 24)
let result3 = geometricMean(4, 10, 16, -24)
let result4 = geometricMean(-24)
print("Arithmetic mean of 5, 10, 15, 20 is: \(result1); should be 12.5")
print("Geometric mean of 4, 10, 16, 24 is: \(result2); should be 11.13...")
print("Geometric mean of 4, 10, 16, -24 is: \(result3); should be -nan")
print("Geometric mean of -24 is: \(result4); should be -24")
