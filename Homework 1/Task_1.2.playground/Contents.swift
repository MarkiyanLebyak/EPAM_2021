import UIKit


func quadratic_roots(a: Double, b: Double, c: Double) {
    if a.isEqual(to: 0.0) {
        print("a should not be zero")
        return
    }
    let discriminant = pow(b, 2) - 4 * a * c
    if discriminant == 0 {
        print("\(a) * x^2 + \(b) * x + \(c) = 0, root = \(-b/(2*a))")
    } else if discriminant < 0 {
        print ("Only complex solutions")
    } else {
        let root1 = (-b + sqrt(discriminant)) / (2 * a)
        let root2 = (-b - sqrt(discriminant)) / (2 * a)
        print("\(a) * x^2 + \(b) * x + \(c) = 0, root1 = \(root1), root2 = \(root2)")
    }
}


quadratic_roots(a: 2, b: 5, c: -3) // Discriminant > 0
quadratic_roots(a: 4, b: -4, c: 1) // Discriminant = 0
quadratic_roots(a: 0, b: -4, c: 1) // a equals zero
quadratic_roots(a: 1, b: -4, c: 5) // Discriminant < 0

